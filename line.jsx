// @flow

import React, {Component} from 'react'
import {withRouter} from 'react-router'
import {translate} from 'react-i18next'
import moment from 'moment'
import {Line} from 'react-chartjs-2'

import {VCS_SLUG} from '../../constants/api'

const MAX_MESSAGE_LENGTH = 40;

const defaultChartOptions = {
	responsive: true,
	title: {
		display: false,
		text: '',
	},
	tooltips: {
		mode: 'index',
		intersect: false,
		position: 'nearest',
		callbacks: {},
	},
	legend: {
		labels: {
			filter: (item/*, chart*/) => item.datasetIndex > 1,
		},
	},
	hover: {
		mode: 'index',
		intersect: false,
	},
	scales: {
		xAxes: [{
			stacked: true,
			scaleLabel: {
				display: false,
				labelString: 'Commits',
			},
			ticks: {
				maxRotation: 0,
				maxTicksLimit: 13,
			},
		}],
		yAxes: [
			{
				stacked: true,
				scaleLabel: {
					display: false,
					labelString: 'Values',
				},
				ticks: {
					beginAtZero: true,
					max: undefined,
					min: 0,
					callback: value => Math.floor(value) === value ? value : undefined, // filter decimal values in y axis
				},
			},
			{
				id: 'threshold',
				display: false,
				type: 'linear',
				position: 'right',
				ticks: {
					beginAtZero: true,
					max: undefined,
					min: 0,
					suggestedMax: 0,
				},
			},
		],
	},
};

const defaultChartData = {
	labels: [],
	datasets: [
		{
			label: 'Issues threshold',
			yAxisID: 'threshold',
			fill: false,
			steppedLine: true,
			backgroundColor: '#f98e0b',
			borderColor: '#f98e0b',
			borderCapStyle: 'butt',
			borderDash: [],
			borderDashOffset: 0.0,
			borderJoinStyle: 'miter',
			pointBorderColor: '#f98e0b',
			pointBackgroundColor: '#f98e0b',
			pointBorderWidth: 1,
			pointHoverRadius: 5,
			pointHoverBackgroundColor: '#f98e0b',
			pointHoverBorderColor: '#f98e0b',
			pointHoverBorderWidth: 2,
			pointRadius: 1,
			pointHitRadius: 10,
			data: [],
		},
		{
			label: 'Errors threshold',
			yAxisID: 'threshold',
			fill: false,
			steppedLine: true,
			backgroundColor: '#870906',
			borderColor: '#870906',
			borderCapStyle: 'butt',
			borderDash: [],
			borderDashOffset: 0.0,
			borderJoinStyle: 'miter',
			pointBorderColor: '#870906',
			pointBackgroundColor: '#870906',
			pointBorderWidth: 1,
			pointHoverRadius: 5,
			pointHoverBackgroundColor: '#870906',
			pointHoverBorderColor: '#870906',
			pointHoverBorderWidth: 2,
			pointRadius: 1,
			pointHitRadius: 10,
			data: [],
		},
		{
			label: 'Errors',
			fill: true,
			backgroundColor: '#e74c3c',
			borderColor: '#e74c3c',
			borderCapStyle: 'butt',
			borderDash: [],
			borderDashOffset: 0.0,
			borderJoinStyle: 'miter',
			pointBorderColor: '#e74c3c',
			pointBackgroundColor: '#e74c3c',
			pointBorderWidth: 1,
			pointHoverRadius: 5,
			pointHoverBackgroundColor: '#e74c3c',
			pointHoverBorderColor: '#e74c3c',
			pointHoverBorderWidth: 2,
			pointRadius: 1,
			pointHitRadius: 10,
			data: [],
		},
		{
			label: 'Warnings',
			fill: true,
			backgroundColor: '#f1c40f',
			borderColor: '#f1c40f',
			borderCapStyle: 'butt',
			borderDash: [],
			borderDashOffset: 0.0,
			borderJoinStyle: 'miter',
			pointBorderColor: '#f1c40f',
			pointBackgroundColor: '#f1c40f',
			pointBorderWidth: 1,
			pointHoverRadius: 5,
			pointHoverBackgroundColor: '#f1c40f',
			pointHoverBorderColor: '#f1c40f',
			pointHoverBorderWidth: 2,
			pointRadius: 1,
			pointHitRadius: 10,
			data: [],
		},
	],
};

type Props = {
	repositoryData: Object,
	height: ?number,
	chartOptions: ?Object,

	// props inherited via decorators
	history: Object,
	location: Object,
	match: Object,
	staticContext: any,
	t: Function,
	tReady: boolean,
	i18n: Object,
};

class LineChart extends Component<Props> {

	static defaultProps = {
		height: 90,
		chartOptions: {},
	};

	getDataForChart(repository: Object) {
		const {t} = this.props;
		let dateTimes = [],
			warnings = [],
			errors = [],
			maxIssues = 0,
			issuesThresholds = [],
			errorsThresholds = [],
			commits = [];

		if (repository.branchVisible !== null) {
			const commitsData = repository.commits[repository.branchVisible.id];
			commits = commitsData.list.slice(commitsData.limit * (commitsData.currentPage - 1), commitsData.limit * (commitsData.currentPage));

			Object.keys(commits).reverse().forEach(index => {
				index = parseInt(index);
				dateTimes.push(moment(new Date(commits[index].datetime)).format('MM/DD HH:mm'));
				warnings.push(commits[index].warningCount || 0);
				errors.push(commits[index].errorCount || 0);
				if (commits[index].errorCount + commits[index].warningCount > maxIssues) {
					maxIssues = commits[index].errorCount + commits[index].warningCount;
				}
			});

			if (commits.filter(commit => commit.options && commit.options.hasOwnProperty('thresholds')).length > 0) {
				issuesThresholds = Object.keys(commits).reverse().map(index => commits[parseInt(index)].options && commits[parseInt(index)].options.hasOwnProperty('thresholds') && commits[parseInt(index)].options.thresholds.hasOwnProperty('total') ? commits[parseInt(index)].options.thresholds.total : null);
				errorsThresholds = Object.keys(commits).reverse().map(index => commits[parseInt(index)].options && commits[parseInt(index)].options.hasOwnProperty('thresholds') && commits[parseInt(index)].options.thresholds.hasOwnProperty('error') ? commits[parseInt(index)].options.thresholds.error : null);
			}
		}

		let data = {
			...JSON.parse(JSON.stringify(defaultChartData)),
			labels: dateTimes,
		};
		data.datasets[2].data = errors;
		data.datasets[2].label = t('errors');
		data.datasets[3].data = warnings;
		data.datasets[3].label = t('warnings');

		if (issuesThresholds.length > 0) {
			maxIssues = Math.pow(10, maxIssues.toFixed(0).length - 1) * Math.ceil(maxIssues / (Math.pow(10, maxIssues.toFixed(0).length - 1)));
			defaultChartOptions.scales.yAxes[0].ticks.max = defaultChartOptions.scales.yAxes[1].ticks.max = maxIssues;
			data.datasets[0].data = issuesThresholds;
			data.datasets[0].label = t('thresholds.total');
		} else {
			defaultChartOptions.scales.yAxes[0].ticks.max = defaultChartOptions.scales.yAxes[1].ticks.max = void 0;
		}

		if (errorsThresholds.length > 0) {
			data.datasets[1].data = errorsThresholds;
			data.datasets[1].label = t('thresholds.error');
		}

		return data;
	}

	render() {
		const {repositoryData, height, chartOptions, history, ...rest} = this.props;

		const data = this.getDataForChart(repositoryData);
		console.log('data', data);

		const commitsData = repositoryData.commits[repositoryData.branchVisible.id];
		const commits = commitsData.list.slice(commitsData.limit * (commitsData.currentPage - 1), commitsData.limit * (commitsData.currentPage));

		const options = {
			...defaultChartOptions,
			onClick: (event: MouseEvent, arr: Array<Object>) => {
				history.push('/' + VCS_SLUG[repositoryData.vcs] + '/' + repositoryData.fullname + '/commits/' + commits[commits.length - arr[0]._index - 1].hash);
			},
			tooltips: {
				...defaultChartOptions.tooltips,
				callbacks: {
					...defaultChartOptions.tooltips.callbacks,
					afterTitle: arr => {
						const message = commits[commits.length - arr[0].index - 1].message;
						return message.length > MAX_MESSAGE_LENGTH ? message.substr(0, MAX_MESSAGE_LENGTH) + '…' : message;
					},
					label: tooltipItem => data.datasets[tooltipItem.datasetIndex].label + ': '+  Intl.NumberFormat(t('numberFormatLocale')).format(tooltipItem.yLabel),
				},
			},
			...chartOptions,
		};

		return (
			<div {...rest}>
				<Line data={data} height={height} options={options} />
			</div>
		);
	}
}

export default withRouter(translate()(LineChart))
