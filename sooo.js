<RepositoryDetailDrawer
					vcsSlug={VCS_SLUG[repository.vcs]}
					fullName={repository.fullname}
					branch={branch}
					className="col col-12 col-md-2"
				/>